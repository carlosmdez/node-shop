const fs = require('fs')
const path = require('path')

const pathFile = path.join(__dirname, '..', 'data', 'cart.json')

module.exports = class Cart {
  static addProduct(id, productPrice) {
    //Fetch the previous cart
    fs.readFile(pathFile, (err, data) => {
      let cart = { products: [], totalPrice: 0 }
      if (!err) {
        cart = JSON.parse(data)
      }
      //Analyze the cart => Finding existing product
      const existingProductIndex = cart.products.findIndex(
        product => product.id === id
      )
      const existingProduct = cart.products[existingProductIndex]
      let updatedProduct
      //Add new product and increase quantity
      if (existingProduct) {
        updatedProduct = { ...existingProduct }
        updatedProduct.quantity++
        cart.products = [...cart.products]
        cart.products[existingProductIndex] = updatedProduct
      } else {
        updatedProduct = { id, quantity: 1 }
        cart.products = [...cart.products, updatedProduct]
      }
      cart.totalPrice += +productPrice
      fs.writeFile(pathFile, JSON.stringify(cart), err => {
        console.log(err)
      })
    })
  }

  static deleteProduct(id, productPrice) {
    //Fetch the previous cart
    fs.readFile(pathFile, (err, data) => {
      if (err) {
        return
      }
      const cart = JSON.parse(data)
      const updatedCart = { ...cart }
      const product = updatedCart.products.find(product => product.id === id)
      updatedCart.products = updatedCart.products.filter(
        product => product.id !== id
      )
      if (product) {
        const productQuantity = product.quantity
        updatedCart.totalPrice -= productPrice * productQuantity
      }
      fs.writeFile(pathFile, JSON.stringify(updatedCart), err => {
        console.log(err)
      })
    })
  }

  static getCart(callback){
    fs.readFile(pathFile, (err, data) => {
      const cart = JSON.parse(data)
      if (cart) {
        callback(cart)
      } else {
        callback(null)
      }
    })
  }
}
