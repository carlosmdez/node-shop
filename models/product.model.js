const fs = require('fs')
const path = require('path')

const Cart = require('./cart.model')

const pathFile = path.join(__dirname, '..', 'data', 'products.json')

const getProductsFromFile = callback => {
  fs.readFile(pathFile, (err, data) => {
    if (err) {
      callback([])
    } else {
      callback(JSON.parse(data))
    }
  })
}

module.exports = class Product {
  constructor(id, title, imageUrl, description, price) {
    this.id = id
    this.title = title
    this.imageUrl = imageUrl
    this.description = description
    this.price = price
  }

  save() {
    getProductsFromFile(products => {
      if(this.id){
        const existingProductIndex = products.findIndex(product => product.id === this.id)
        const updatedProducts = [...products]
        updatedProducts[existingProductIndex] = this
        fs.writeFile(pathFile, JSON.stringify(updatedProducts), err => {
          console.log(err)
        })
      }
      else {
        this.id = Math.random().toString()
        products.push(this)
        fs.writeFile(pathFile, JSON.stringify(products), err => {
          console.log(err)
        })
      }
    })
  }

  static deleteById(id){
    getProductsFromFile(products => {
      const product = products.find(product => product.id === id)
      const existingProductIndex = products.findIndex(product => product.id === id)
      const updatedProducts = [...products]
      updatedProducts.splice(existingProductIndex, 1)
      fs.writeFile(pathFile, JSON.stringify(updatedProducts), err => {
        if (!err) {
          Cart.deleteProduct(id, product.price)
        }
      })
    })
  }

  static fetchAll(callback) {
    getProductsFromFile(callback)
  }

  static findById(id, callback) {
    getProductsFromFile(products => {
      const product = products.find(prod => prod.id === id)
      callback(product)
    })
  }
}
