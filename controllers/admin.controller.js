const Product = require('../models/product.model')

exports.getAddProduct = (req, res) => {
  res.render('admin/edit-product', {
    path: '/admin/add-product',
    pageTitle: 'Add Product',
    editing: false
  })
}

exports.postAddProduct = (req, res) => {
  const {title, imageUrl, description, price} = req.body
  const product = new Product(null, title, imageUrl, description, price)
  product.save()
  res.redirect('/')
}

exports.getEditProduct = (req, res) => {
  const editMode = (req.query.edit === 'true')
  //This is redundant but I want to practice query parameters
  if (!editMode) {
    return res.redirect('/')
  }
  const prodId = req.params.id
  Product.findById(prodId, product => {
    if (!product) res.redirect('/')
    res.render('admin/edit-product', {
      path: '/admin/edit-product',
      pageTitle: 'Edit Product',
      editing: editMode,
      product
    })
  })
}

exports.postEditProduct = (req, res) => {
  const {productId, title, imageUrl, description, price} = req.body
  const updatedProduct = new Product(productId, title, imageUrl, description, price)
  updatedProduct.save()
  res.redirect('/admin/products')
}

exports.postDeleteProduct = (req, res) => {
  const prodId = req.body.productId
  Product.deleteById(prodId)
  res.redirect('/admin/products')
}

exports.getProducts = (req, res) => {
  Product.fetchAll(products => {
    res.render('admin/admin-products', { products, path: '/admin/products', pageTitle: 'Admin Products' })
  })
}
