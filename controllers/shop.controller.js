const Product = require('../models/product.model')
const Cart = require('../models/cart.model')

exports.getIndex = (req, res) => {
  Product.fetchAll(products => {
    res.render('shop/index', { products, path: '/', pageTitle: 'Shop' })
  })
}

exports.getProducts = (req, res) => {
  Product.fetchAll(products => {
    res.render('shop/product-list', { products, path: '/products', pageTitle: 'All Products' })
  })
}

exports.getProduct = (req, res) => {
  const prodId = req.params.productId
  Product.findById(prodId, (product) => {
    res.render('shop/product-detail', { product, path: '/products', pageTitle:'Product Detail'})
  })
}

exports.getOrders = (req, res) => {
  res.render('shop/orders', { path: '/orders', pageTitle: 'My Orders' })
}

exports.getCart = (req, res) => {
  Cart.getCart(cart => {
    Product.fetchAll(products => {
      const cartProducts = []
      for (product of products) {
        const cartProductData = cart.products.find(prod => prod.id === product.id)
        if (cartProductData) {
          cartProducts.push({productData: product, quantity: cartProductData.quantity})
        }
      }
      res.render('shop/cart', { path: '/cart', pageTitle: 'My Cart', products: cartProducts })
    })
  })
}

exports.postCart = (req, res) => {
  const prodId = req.body.productId
  Product.findById(prodId, product => {
    Cart.addProduct(prodId, product.price)
  })
  res.redirect('/cart')
}

exports.postCartDeleteItem = (req, res) => {
  const prodId = req.body.productId
  Product.findById(prodId, product => {
    Cart.deleteProduct(prodId, product.price)
    res.redirect('/cart')
  })
}

exports.getCheckout = (req, res) => {
  res.render('shop/checkout', { path: '/checkout', pageTitle: 'Checkout' })
}

