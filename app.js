const path = require('path')
const express = require('express')
const app = express()

app.set('view engine', 'pug')
app.set('views', 'views')

// Routes //
const adminRoutes = require('./routes/admin.routes')
const shopRoutes = require('./routes/shop.routes')

//Controllers //
const errorController = require('./controllers/error.controller')

// Middlewares //
app.use(express.urlencoded({ extended: false }))

// Static files //
app.use(express.static(path.join(__dirname, 'public')))

app.use('/admin', adminRoutes)
app.use(shopRoutes)

app.use(errorController.get404)

app.listen(3000)
